## Choco Server
#### Last Updated - 11/10/2020

## How 2 Install

There are 2 options to install: [directly modifying the .minecraft folder](https://gitlab.com/LiefEriccson/dancing-arks-mc-server/-/blob/master/README.md#setup-instructions-through-the-minecraft-folder) or [using MultiMC](https://gitlab.com/LiefEriccson/dancing-arks-mc-server/-/blob/master/README.md#setup-instructions-through-multimc). I will go through both options here!

### Setup instructions through the `/.minecraft` folder

**First we need to make sure we have a cleared Minecraft folder!**
- If you have never installed Minecraft... well now's the time! Go ahead and go through the default installation process with the official client.
  - Go ahead and launch the game once to make sure it launches without any issues and then close it.
- If you DO have minecraft installed already, then we'll have to clear the contents of the folder. 
  - Navigate to `.minecraft` folder
    - For Windows, press `WIN + R` and type `%APP_DATA%`
  - Clear the existing contents, if any, by deleting it
    - If you wanna retain your login info and older configurations, don't remove:
      - `.ReAuth`
      - `launcher_profiles.json`, `launcher_settings.json`, `servers.dat`
      - (if these exist) `usercache.json`, `usernamecache.json`
      - The entire `webcache` folder

**Now we need to install Forge**
- Download [Forge here](https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.15.2-31.2.45/forge-1.15.2-31.2.45-installer.jar) (Clicking on this will immediately download the `.jar`)
- Run `forge-1.15.2-31.2.45-installer.jar` and then click on `Install as Client`

  ![forge-installer](https://i.imgur.com/jjjkNKR.png)

- Launch Minecraft again from the official client
- Press `Installations` at the top and press `+ New`

  ![Step 1](https://i.imgur.com/lHd3HyW.png)

- On the following dialog, name the installation whatever you'd like and make sure to select the version that has `forge-1.15.2-31.2.45` in it.

- Press `More Options`
- Under `JVM Arguments`, set memory settings to `5G` (If you plan on using shaders, set it to `7G` or `8G`)

  ```
  -Xmx5G -XX:+UnlockExperimentalVMOptions -XX:+UseG1GC -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=16M
  ```

  ![Step 2](https://i.imgur.com/4LikBZB.png)

- Press `Create / Save`

- Go back to `Play` and then press the dropdown arrow next to the green Play button

  ![Step 3](https://i.imgur.com/8AUKOq4.png)
  
- Select the newly created installation

And you are done installing Minecraft + Forge from scratch! [Click here to go to next step](https://gitlab.com/LiefEriccson/dancing-arks-mc-server/-/blob/master/README.md#installing-the-mods)


### Setup instructions through MultiMC
MultiMC is a nifty 3rd party Minecraft launcher that lets you save multiple instances of Minecraft if you want to concurrently have multiple versions of Minecraft. 

[Download MultiMC here](https://multimc.org/)

Extract MultiMC to a location that you will remember and don't mind having large files being there. Once that is done, follow the next steps:

- Open up MultiMC and click on `Add Instance`
- Give it a nice name and then make sure to select the `1.15.2` version!
- Click okay
  
  ![Step 1](https://i.imgur.com/uZ7sumS.png)

- Let it complete the setup process. Once it is done, right click on the instance and click on `Edit Instance`. It should open up a new window. 
- Click on the `Version` icon on the top left. Then click on `Install Forge` on the right side of the window. Make sure you install `31.2.45`. It should be the very top of as of this latest README update!

  ![Step 2](https://i.imgur.com/DKbBk7F.png)

- **IMPORTANT** Make sure the order of version goes (from top to bottom): `LWJGL 3`, `Minecraft`, `Forge`
- Now go to the `Settings` icon on the left side of the window. 
- If you already set the proper RAM requirements during installation of the instance, then you can ignore the `Memory` section. Otherwise, Set the `maximum memory allocation` to the amount you feel comfortable with (This modpack probably uses around 4GB. Make it larger if you wanna run shaders!)
- Now, paste these arguments into the `Java arguments` section: `-XX:+UnlockExperimentalVMOptions -XX:+UseG1GC -XX:G1NewSizePercent=20 -XX:G1ReservePercent=20 -XX:MaxGCPauseMillis=50 -XX:G1HeapRegionSize=16M`

  ![Step 3](https://i.imgur.com/pz0xNzS.png)

- Now you can close the window C:

And you are done installing Minecraft + Forge through MultiMC!

You can verify if your installation looks good if you launch Minecraft and see this at the bottom left:

  ![Successful base launch](https://i.imgur.com/09FbI0t.png)

### Installing the mods

- Once you completed the installtion, navigate your way to your `.minecraft` folder
  - For Windows, press `WIN + R` and type `%APPDATA%`
- If you are using MultiMC, right click on the instance and click `Minecraft Folder`

- Copy ALL the contents of the `CLIENT FILES` folder into your `minecraft` folder

- Verify that the game loads in the mods and configs correctly!

- Go to multiplayer and DM me for the server info c:


### Optional - symlinking the folders to make updating easy:
Symlinking is creating a symbolic link from one file/folder to another file/folder. This essentially makes it so updates on the source folder will automatically update the symlinked folder! No more copy pasta if there is a chance this server updates! This will work best if you `git cloned` this repository instead of downloading the folders manually. 

On Windows (using your respective drive and directories, of course), open up `cmd` with Admin Privileges (right click -> run as admin or CTRL+SHIFT+Enter on the start menu if you just type in `cmd` on the search bar). The command prompt window will say `Administrator` at the top left if you successfully launch it as admin. 
  
The command looks like this: `mklink /D "target" "source"`, where target is the minecraft installation and source is the downloaded files from here. You need QUOTES around the "target" and "source" if your file path contains spaces.  

Additionally, ensure that your minecraft folder does NOT contain a `mods` and `config` folder already. If it does, delete it before running the command.

Here is an example command:
    
    
    mklink /D C:\Users\<Your Username>\AppData\Roaming\.minecraft\mods "C:\dancing-arks-mc-server\CLIENT FILES\mods"

    mklink /D C:\Users\<Your Username>\AppData\Roaming\.minecraft\config "C:\dancing-arks-mc-server\CLIENT FILES\config"
    

